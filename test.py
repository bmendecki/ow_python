from flask import Flask, render_template, request
from weather import Weather

app = Flask(__name__)


@app.route('/')
def home():
    w1 = Weather("PL")
    return render_template('index.html', desc=w1.desc, temp=w1.temp, city=w1.city)

@app.route('/', methods=['GET', 'POST'])

def send_lang():

    lang = request.form['lang']
    w1 = Weather(lang)
    return render_template('index.html', desc=w1.desc, temp=w1.temp, city=w1.city)

if __name__ == '__main__':

    app.run(debug=True)
