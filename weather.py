#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import os
import time
import json
from capitals import CAPITALS


class Weather:
    "klasa pogodowa w Pythonie"
    USERID = "xxxxxxxxxxxxxxxxxxxxxxxxx"
    PATH = "stored/"

    def __init__(self,ctry):
        self.getdata(ctry)

    def getdata(self,ctry):
        cityid = CAPITALS[ctry]
        filename = os.getcwd() + "/store/" + ctry + ".txt"

        url = "http://api.openweathermap.org/data/2.5/forecast/city?id={cityid}&units=metric&APPID={userid}&format=json".format(cityid=cityid, userid=Weather.USERID)

        now = time.time()
        localtime = time.asctime( time.localtime(now))

        if not os.path.isfile(filename):
            r = requests.get(url)
            weather = r.json()
            print("nie ma pliku, tworzę i zapisuję")
            with open(filename, "w") as file_:
                json.dump(weather, file_)
                print("Zapisano!")
        else:
            print("jest")
            lstm = os.path.getmtime(filename)
            if (now - lstm) > 10:
                r = requests.get(url)
                weather = r.json()
                with open(filename, "w") as file_:
                    json.dump(weather, file_)
                    print("Zapisano!")
            else:
                print("jeszcze w zakresie")

        with open(filename) as file_:
            weather = json.load(file_)

        Weather.desc = weather["list"][3]["weather"][0]["description"]
        Weather.temp = weather["list"][3]["main"]["temp"]
        Weather.city = weather["city"]["name"]


# w1 = Weather("FR")
